#!/usr/bin/env python3
# encoding: UTF-8

# This file is part of Turberfield.
#
# Turberfield is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Turberfield is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Turberfield.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import functools
import importlib
import importlib.resources
import pathlib
import shutil
import sys

from turberfield.dialogue.model import Model
from uncarved.lockdown.presenter import Presenter
from uncarved.lockdown.site import Site
from uncarved.lockdown.theme import Theme
import uncarved.lockdown.themes.carving.render as render

"""
<html>
<link rel="alternate" type="application/json" title="JSON Feed" href="https://www.example.com/feeds/all.json">
<link rel="alternate" type="application/json" title="JSON Feed" href="https://www.example.com/feeds/my_category.json">
</html>
"""


class Carving(Theme):

    definitions = {
        "creamy": "hsl(50, 0%, 100%, 1.0)",
        "pebble": "hsl(13, 0%, 30%, 1.0)",
        "claret": "hsl(13, 80%, 55%, 1.0)",
        "blonde": "hsl(50, 80%, 35%, 1.0)",
        "bubble": "hsl(320, 100%, 50%, 1.0)",
        "forest": "hsl(76, 80%, 35%, 1.0)",
        "rafter": "hsl(36, 20%, 18%, 1.0)",
        "titles": '"AA Paro", sans-serif',
        "mono": ", ".join([
            "SFMono-Regular", "Menlo", "Monaco",
            "Consolas", '"Liberation Mono"',
            '"Courier New"', "monospace"
        ]),
        "system": ", ".join([
            "BlinkMacSystemFont", '"Segoe UI"', '"Helvetica Neue"',
            '"Apple Color Emoji"', '"Segoe UI Emoji"', '"Segoe UI Symbol"',
            "Arial", "sans-serif"
        ]),
    }

    def __exit__(self, exc_type, exc_val, exc_tb):
        for d in ("css",):
            with importlib.resources.path("uncarved.lockdown.themes.carving", d) as path:
                shutil.copytree(path, self.root.joinpath(d), dirs_exist_ok=True)

        return False

    def render_pages(self, pages, *args, **kwargs):
        for page in pages:
            presenter = Presenter(page.model)
            for n, frame in enumerate(presenter.frames):
                frame = presenter.animate(frame)
                next_frame = self.frame_path(page, n + 1).relative_to(page.path).as_posix()
                text = render.frame_to_text(frame)
                html = render.body_html(
                    next_=next_frame if n < len(presenter.frames) -1 else None,
                    refresh=Presenter.refresh_animations(frame) if presenter.pending else None,
                    title=page.title.capitalize(),
                ).format(
                    render.dict_to_css(self.definitions),
                    render.frame_to_html(frame, title=page.title.capitalize(), final=not presenter.pending)
                )
                path = self.frame_path(page, n)
                yield page._replace(ordinal=n, text=text, html=html, path=path)

    def render_feeds(self, pages, *args, **kwargs):

        for n, title in enumerate(("index",)):
            yield Site.Page(
                key=(n,), ordinal=0, script_slug=None, scene_slug=None, lifecycle=None,
                title=title.capitalize(),
                model=None,
                text="This is the index page.",
                html=render.body_html(title=title).format(
                    render.dict_to_css(self.definitions),
                    render.feed_to_html(pages, self.root, self.cfg),
                ),
                path=self.root.joinpath(title).with_suffix(".html"),
                feeds=tuple(), tags=tuple(),
            )

    def render(self, pages, *args, **kwargs):
        self.root = self.root or pathlib.Path(*min(i.path.parts for i in pages))
        pages = list(self.render_pages(pages, *args, **kwargs))
        yield from pages
        yield from self.render_feeds(pages, *args, **kwargs)

def theme(path, *args, **kwargs):
    return Carving(path, *args)


def parser():
    rv = argparse.ArgumentParser()
    rv.add_argument(
        "--output", required=False, default=None, type=pathlib.Path,
        help="Set directory for output."
    )
    return rv

def main(args):
    print("Hi")

if __name__ == "__main__":
    p = parser()
    args = p.parse_args()
    rv = main(args)
    sys.exit(rv)
