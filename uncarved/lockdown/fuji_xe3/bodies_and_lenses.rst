:tag: Fuji
:feed: photo

Getting into Fuji
=================

Pitch
-----

The Fuji X-E3 is small. It's an affordable body for Fuji X mount lenses.
But can you use it for Video?

Titles
------

.. No clue yet

Background
----------

I got my Fuji X-E3 in October of 2019. And at the time I had no plans at all for using it for video.

I was working on a personal project; a web project which needed some illustrations. I soon realised my own
drawing skills weren't adequate for the task. And so I turned to the idea of using photos instead.

What I wanted
-------------

So at the time I wanted to start doing monochrome still photography. From a skill base of absolute zero.

I was after moody architectural scenes. And maybe some head and shoulders character shots.

And I had in mind a highly stylised, film noir look.

I knew I had a lot of learning ahead of me, and I would need a lot of practice with the camera.
And so I wanted something small that I could take with me conveniently without it being a burden.
Something I would enjoy using, something I'd want to spend time with.

Hence Fuji X-E3
---------------

So I got an X-E3 body for about �600. And the Fuji 23mm F2 lens for �400 and something. Then shortly after I
got the 27mm F2.8 pancake lens for just over �200.

And that was my setup. It all went into the smallest of bags. Most of the time I left the pancake lens on and I
would just pop the X-E3 into a jacket pocket when I went out somewhere. You need decent-sized pockets, but you
can actually do that with the X-E3.

And very slowly I learned how to use it.

I watched a lot of Sean Tucker's videos from back when he was using Fuji. And I mainly used the Acros film
simulation in order to learn about black and white exposure and shadows and so on.

But Video though
----------------

What about video though?

Well on the X-E3 you are limited to only 10 minutes at 4K. You get 15 minutes at 1080p. There's no
slow motion. So not good start.
