:author: JunkDLC
:made_at: 2020-08-31
:edit_at: 2020-08-31
:drop_at: 2020-09-30

.. entity:: AYNUK
   :types: turberfield.punchline.types.Eponymous


Word power
==========

Text mode
---------

[AYNUK]_

    Orroight!

    Look here_.

Further
-------

Look here_.

.. _here: https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html
